# ACME Users

API and client for listing users.

## Requirements

LAMP stack and Composer installed.

## Installation

### Add virtual hosts for two new sites.

Example configation is bellow:
    
__File: /etc/apache2/sites-available/acme.conf__
   
    <VirtualHost *:80>
	    ServerAdmin webmaster@localhost
        ServerName acme.local
        ServerAlias www.acme.local
        DocumentRoot /var/www/cors/client/public
    
        <Directory /var/www/cors/client/public>
            AllowOverride All
            Order Allow,Deny
            Allow from All
            FallbackResource /index.php
        </Directory>
    
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
    </VirtualHost>

__File: /etc/apache2/sites-available/acme-api.conf__

    <VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName acme-api.local
        ServerAlias www.acme-api.local
        DocumentRoot /var/www/cors/server/public
    
        <Directory /var/www/cors/server/public>
            AllowOverride All
            Order Allow,Deny
            Allow from All
            FallbackResource /index.php
            
            RewriteEngine on
            RewriteCond %{HTTP:Authorization} ^(.*)
            RewriteRule .* - [e=HTTP_AUTHORIZATION:%1]
        </Directory>
    
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
     </VirtualHost>

__File: /etc/hosts__

    127.0.0.1	acme.local
    127.0.0.1	acme-api.local

Enable new sites and restart Apache

    sudo a2ensite acme
    sudo a2ensite acme-api
    sudo /etc/init.d/apache2 reload


### Clone repositories

    mkdir /var/www/cors
    cd /var/www/cors
    git clone git@bitbucket.org:ondrusek/cors-server.git server
    git clone git@bitbucket.org:ondrusek/cors-client.git client

### Composer install

    cd /var/www/cors/server
    composer install
    cd /var/www/cors/client
    composer install

### Set-up database credentials
Create databse **acme**. Replace **user** and **password** in **.env** file:

    DATABASE_URL=mysql://user:password@127.0.0.1:3306/acme

### Set-up LexikJWTAuthenticationBundle
Generate the SSH keys:

    cd /var/www/cors/application/server
    mkdir -p config/jwt
    openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
    openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout

Add passphrase in __.env__:
    
    JWT_PASSPHRASE=heslo

### Load fixtures
    ./reload.sh
    
Fixture usernames are selected Czech names. Password is same as username.
    