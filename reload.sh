#!/bin/sh

php bin/console cache:pool:clear cache.global_clearer

#php bin/console doctrine:database:drop --force

php bin/console doctrine:database:create

php bin/console doctrine:schema:update --force

#php bin/console doctrine:fixtures:load -n
