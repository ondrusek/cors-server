<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class ApiController extends AbstractController
{
    /**
     * @Route("/users/{offset}", name="users")
     */
    public function usersAction(int $offset = 1)
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAllOrderedOffset($offset);

        $usersResponse = [];

        foreach ($users as $user) {
            $usersResponse[] = ['id' => $user['id'], 'username' => $user['username']];
        }

        $count = $this->getDoctrine()->getRepository(User::class)->count([]);

        return new JsonResponse(['users' => $usersResponse, 'offset' => $offset, 'count' => $count]);
    }
}